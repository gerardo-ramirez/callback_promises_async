const {
    taskOne,
    taskTwo
} = require('./tasks');

const main =async ()=>{
    const valor1= await taskOne();
const valor2 = await taskTwo();

console.log("valor uno: ", valor1);
console.log("valor dos: ", valor2);

}

main();